FROM node:lts-alpine

WORKDIR /app

COPY package*.json ./

COPY client/package*.json client/
RUN npm run install-client

COPY server/package*.json server/
RUN npm run install-server

COPY client/ client/
RUN npm run build --prefix client

COPY server/ server/

USER root
RUN chmod 777 ./server/node_modules
RUN chmod 777 ./client/node_modules
RUN chmod 777 ./client/node_modules/webpack-dev-server/node_modules
RUN chmod 777 ./client/node_modules/watchpack-chokidar2/node_modules
RUN chown node:node .

USER node

CMD ["npm", "start"]
