# webrtc-chatApp
Find this app at: https://murmuring-shore-46191.herokuapp.com/


## Getting started

<pre>To run this project use the below steps after you have cloned the above repo:
1. sudo heroku login
2. sudo heroku container:login
3. sudo heroku container:push web
4. sudo heroku container:release web
5. sudo heroku open
</pre>
