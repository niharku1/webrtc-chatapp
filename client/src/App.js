import './App.css';
import React,{Component} from 'react'
import main from './js/main';

class App extends Component {

  constructor(){
    super();
    this.state = {mount:false}
  }

  componentDidMount(){
    this.setState({
      mount:true
    })
  }

  render(){
  return (
    <div className="App">
      <h1>Realtime communication with WebRTC</h1>

        <div id="videos">
          <video id="localVideo" autoPlay muted></video>
          <video id="remoteVideo" autoPlay></video>
        </div>

      {this.state.mount === true ? main() : <></>}
    </div>
  );
  }
}

export default App;
