import { io } from "socket.io-client";
import turnConfig from "./config";

function main(){

    let isChannelReady = false;
    let isStarted = false;
    let isInitiator = false;
    let localStream;
    let remoteStream;
    let localVideo;
    let remoteVideo;
    let pc;
    const pcConfig = turnConfig;

    const mediaConstraint = {
        audio:true,
        video:true
    }

    let room = prompt("Enter room name")
    let socket = io.connect()

    if(room !== '') {
        socket.emit('create or join', room)
    }

    socket.on('created', () => {
        isInitiator = true
    })
    
    socket.on('join', () => {
        isChannelReady = true
    })

    socket.on('joined', () => {
        isChannelReady = true
    })

    socket.on('full', function(room) {
        console.log('Room ' + room + ' is full');
    });

  
    const handleRemoteHangup = () => {
        console.log('Session terminated.');
        stop();
        isInitiator = false;
      }
      
    const stop = () => {
        isStarted = false;
        pc.close();
        pc = null;
    }
    const sendMessage = (message, room) => {
        socket.emit('message', message, room)
    }
    const setLocalDescriptionAndMessage = (sessionDescription) => {
        pc.setLocalDescription(sessionDescription)
        sendMessage(sessionDescription, room)
    }
    const handleLocalDescriptionError = (e) => {
        console.log('Following error occurred while setting local description: ', e)
    }
    const handleError = (e) => {
        console.log('Following error occurred while creating Answer: ', e)
    }

    const offerCreator = () => {
        pc.createOffer(setLocalDescriptionAndMessage, handleLocalDescriptionError)
    }


    const answerCreator = () => {
        pc.createAnswer().then(setLocalDescriptionAndMessage).catch(handleError)
    }

    const handleIceCandidate = (e) => {
        if(e.candidate) {
            sendMessage({
                type: 'candidate',
                label: e.candidate.sdpMLineIndex,
                id: e.candidate.sdpMid,
                candidate: e.candidate.candidate
            }, room)
        }else{
            console.log('End of candidates.');
        }
    }

    const handleAddRemoteStream = (e) => {
        remoteStream = e.stream
        remoteVideo.srcObject = remoteStream
    }
    const handleRemoveRemoteStream = () => {
        console.log('Remote stream removed.')
    }

    const createPeerConnection = () => {
        try{
            pc = new RTCPeerConnection(pcConfig);
            pc.onicecandidate = handleIceCandidate
            pc.onaddstream = handleAddRemoteStream
            pc.onremovestream = handleRemoveRemoteStream
        }catch(e){
            console.log('cannot create RTC Peer connection object.')
        }
    }


    const maybeStart = () => {
        if(!isStarted && typeof localStream !== undefined && isChannelReady) {
            createPeerConnection()
            pc.addStream(localStream)
            isStarted = true
            if (isInitiator) {
                offerCreator()
            }
        }
    }


    socket.on('message', (message, room) => {
        if(message === 'got user media'){
            maybeStart()
        }
        else if(message.type === 'offer'){
            if(!isInitiator === true && !isStarted === true){
                maybeStart()
            }
            pc.setRemoteDescription(new RTCSessionDescription(message))
            answerCreator() 
        }
        else if(message.type === 'answer' && isStarted) {
            pc.setRemoteDescription(new RTCSessionDescription(message))
        }
        else if(message.type === 'candidate' && isStarted) {
            let candidate = new RTCIceCandidate({
                sdpMLineIndex:message.label,
                candidate:message.candidate
            })
            pc.addIceCandidate(candidate)
        }
        else if(message === 'bye' && isStarted) {
            handleRemoteHangup();
        }
    })

        localVideo = document.getElementById('localVideo')
        remoteVideo = document.getElementById('remoteVideo')
    
    
        const gotLocalMediaStream = (mediaStream) => {
            localStream = mediaStream
            localVideo.srcObject = mediaStream
            sendMessage('got user media', room)
            if(isInitiator){
                maybeStart()
            }
        }

        const handleMediaError = (e) => {
            console.log('Following Error occurred while getting media devices: ' + e)
        }
    
        navigator.mediaDevices.getUserMedia(mediaConstraint).then(gotLocalMediaStream).catch(handleMediaError)



    // const startButton = document.getElementById('startButton')
    // const callButton = document.getElementById('callButton')
    // const hangupButton = document.getElementById('hangupButton')

    // startButton.addEventListener('click', startAction)
    // callButton.addEventListener('click', callAction)
    // hangupButton.addEventListener('click', hangupAction)

}

export default main