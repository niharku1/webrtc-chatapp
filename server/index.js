'use strict';
const os = require('os')
const express = require('express')
const http = require('http')
const path = require('path')
const socketio = require('socket.io')

const app = express()

app.use(express.static('public'))
app.get('/', (req, res) => {
   res.sendFile(path.join(__dirname, 'public', 'index.html'))
});

const server = http.createServer(app).listen(process.env.PORT || 8000)
console.log('App is running on port: ', process.env.PORT)
var io = socketio(server);
const users = []

io.sockets.on('connection', function(socket) {

  // convenience function to log server messages on the client
  function log() {
    var array = ['Message from server:'];
    array.push.apply(array, arguments);
    socket.emit('log', array);
  }

  socket.on('message', function(message, room) {
    log('Client said: ', message);
    // for a real app, would be room-only (not broadcast)
    //socket.broadcast.emit('message', message);
    socket.to(room).emit('message', message, room);
  });

  socket.on('create or join', function(room) {
    log('Received request to create or join room ' + room);
   
    //Steps followed below:
    //converting Map object into Object whose keys are strings and values are Set() type.
    // io.sockets.adapter.rooms gives rooms created and the clients in them
    // obj[room].size gives number of clients in a particular room
    let obj = Array.from(io.sockets.adapter.rooms).reduce((obj, [key, value]) => (
        Object.assign(obj, { [key]: value })
    ), {});
    let numClients = obj[room] !== undefined ? obj[room].size : 0 

    log('Room ' + room + ' now has ' + numClients + ' client(s)');

    if (numClients === 0) {
      socket.join(room);
      log('Client ID ' + socket.id + ' created room ' + room);
      socket.emit('created', room, socket.id);

    } else if (numClients === 1) {
      log('Client ID ' + socket.id + ' joined room ' + room);
      io.sockets.in(room).emit('join', room);
      socket.join(room);
      socket.emit('joined', room, socket.id);
      io.sockets.in(room).emit('ready');
    } else { // max two clients
      socket.emit('full', room);
    }
  });

  socket.on('ipaddr', function() {
    var ifaces = os.networkInterfaces();
    for (var dev in ifaces) {
      ifaces[dev].forEach(function(details) {
        if (details.family === 'IPv4' && details.address !== '127.0.0.1') {
          socket.emit('ipaddr', details.address);
        }
      });
    }
  });

  socket.on('bye', function(){
    console.log('received bye')
  })

});
